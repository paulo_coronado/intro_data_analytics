#Script to analice a dataset from Bogota`s OpenData Hub.

# 1. Download the source dataset in csv format.
# https://datosabiertos.bogota.gov.co/dataset/predios-bogota

#2. Load the packages that we need for the analysis

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

#2. Load the data using read_csv from pandas
#miData=pd.read_csv("predios_csv_1221.csv")
#myData=pd.read_csv("predios_csv_1221.csv", on_bad_lines='skip', delimiter=';',skiprows=[i for i in range(1,50000)], nrows=20000)
#myData=pd.read_csv("predios_csv_1221.csv", on_bad_lines='skip', delimiter=';',nrows=10000)
#2.A. Cuando el conjunto de datos es grande se recomienda colocar el tipo de dato que tiene cada columna
# Tip: el diccionario de dtype puede crearse al vuelo con un conjunto reducido de datos.

#Converters
#creating functions to clean the columns
def CCleanData(x):
    x=x.replace(',','.')
    x=x.replace(' ','')
    try: 
        x=round(float(x), 3)
    except:
        x=0
    
    return x


myData=pd.read_csv("predios_csv_1221.csv", on_bad_lines='skip', delimiter=';',
converters={
    'PreAConst':CCleanData, 
    'PreATerre':CCleanData, 
    }, 
nrows=1000)
'''                 
myData= pd.read_csv('predios_csv_1221.csv', on_bad_lines='skip', delimiter=';',
                 dtype={
                     'PreCBarrio': int,
                     'PreCManz': int
                 })

'''

#3. Show a portion of the data
print(myData.head())
#3A. Show a column
print(myData['PreAConst'])
#3B. Get one row
#iloc: search by row`s index
#loc: search by row`s label
row=myData.iloc[1]
print(row)
#iloc return a serie, an unidimensional array whose rows are labeled with the name of each column
#print (isinstance(row, pd.Series))

#4. Dimension of the dataframe
print(myData.shape)

#5. Information about the columns of the dataframe

print(myData.info())

#6. Statistics

print(myData.describe())

#6.A. Statitics of a numeric column
print(myData['PreAConst'].describe())
#6.B. Specific statistics
print(myData['PreAConst'].median())
# Variance: The expectation of the squared deviation of a random variable from its population mean or sample mean. 
# Variance is a measure of dispersion, meaning it is a measure of how far a set of numbers is spread out from their 
# average value. 
print(myData['PreAConst'].var())

#7. Plotting 
'''
plt.figure()
myData['PreAConst'].plot()
plt.figure()
myData['PreAConst'].diff().hist()
'''

#7.A Subplots
#To create a figure and a matrix of axes
'''
fig, axes = plt.subplots(nrows=2, ncols=2)
myData['PreAConst'].plot(ax=axes[0,0])
compositeFigure=myData['PreAConst'].plot(kind='density', ax=axes[0,1])
compositeFigure.axvline(myData['PreAConst'].mean(), color="red")
compositeFigure.axvline(myData['PreAConst'].median(), color="#00FF00")

myData['PreAConst'].plot(kind='box', ax=axes[1,0], vert=False)
myData.plot.scatter(x='PreAConst', y='OBJECTID',ax=axes[1,1])
plt.show()
'''
'''
myData[['PreAFachad','PreACubier', 'PreAPisos', 'PrePuntaje']].plot(kind='box', subplots=True, layout=(2,2))
plt.show()
'''

#8. Categorical analysis
print(myData['PreNBarrio'].value_counts())
'''
plt.figure()
myData['PreNBarrio'].value_counts().plot(kind='pie')
plt.figure()
myData['PreNBarrio'].value_counts().plot(kind='bar', ylabel='Cantidad de Predios')
plt.show()
'''

#9. Correlation
corr=myData.corr()
#Data
#print(corr.to_string())
#Graphical depiction
'''
myMatrixGraph=plt.figure()
plt.matshow(corr, myMatrixGraph.number, cmap='Blues')
plt.xticks(range(len(corr.columns)), corr.columns, rotation='vertical')
plt.yticks(range(len(corr.columns)), corr.columns)
plt.show()
'''
'''
boxData=myData[['PreNBarrio','PreAConst']]
boxData.plot(by='PreNBarrio', kind='box')
plt.xticks(rotation='vertical')
plt.show()
print(boxData)
'''

#10. Add columns to dataframe
#Using numpy`s where function: where(condition, [x, y]), return elements chosen from x or y depending on condition.
myData['PreBuildPercent']= np.where(myData['PreATerre']!=0, myData['PreAConst']/myData['PreATerre'], 0)
#Using lambda functions
myData['PreBuildPercent']= myData.apply(lambda x: x.PreAConst/x.PreATerre if x.PreATerre!=0 else 0, axis=1)
#Using direct operation
myData['PreBuildPercent']= myData['PreAConst']/myData['PreATerre']
#Using custom function
'''
def myFunc(x):
    if x.PreATerre!=0:
        return x.PreAConst/x.PreATerre
    else:
        return 0
myData['PreBuildPercent']= myData.apply(myFunc, axis=1)
for x in myData['PreBuildPercent']:
    print(x)
myData['PreBuildPercent'].plot(kind='density')
plt.show()
'''

#11. Modify the values of an entire column 
myData['PreBuildPercent']*=1.2

#12. Select the rows that meet some criteria
# loc selects rows and columns using labels, iloc selects rows and columns using integer positions
#rows that meet some criteria and all columns
fdata=myData.loc[myData['PreNBarrio']=='LA ESPERANZA']
print(fdata)

fdata=myData.loc[(myData['PreNBarrio']=='LA ESPERANZA') | (myData['PreNBarrio']=='PARAMO IV')]
print(fdata)

#Rows that meet some criteria and only some columns
fdata=myData.loc[myData['PreNBarrio']=='LA ESPERANZA', 'PreCBarrio']
print(fdata)

fdata=myData.loc[myData['PreNBarrio']=='LA ESPERANZA', ['PreCBarrio', 'PreCManz']]
print(fdata)

#Select all rows of one column
fdata=myData.loc[:,'PreNBarrio']
print(fdata)

#Select all rows of one column and all columns behind it
fdata=myData.loc[:,:'PreNBarrio']
print(fdata)

#Select all rows of two columns
fdata=myData.loc[:,['PreNBarrio', 'OBJECTID']]
print(fdata)

#Select all rows behind some label of two columns
fdata=myData.loc[:20,['PreNBarrio', 'OBJECTID']]
print(fdata)




